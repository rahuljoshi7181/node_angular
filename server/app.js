const express       =   require('express');
const bodyParser    =   require('body-parser');
const cors          =   require('cors');

const app           =   express();

const routes        =   require('./routes/rout');
 
const db            =   require('./util/db');

app.use(cors());
/*
app.use((req,res,next)=>{
   res.setHeader('Access-Control-Allow-Origin','*'); // * is use for all domain
   res.setHeader('Access-Control-Allow-Method','GET,POST,PUT,PATCH,DELETE');
   res.setHeader('Access-Control-Allow-Headers','Content-Type:Authorization');
   next();
});
*/
app.use(bodyParser.json());
app.use('/',routes);



app.listen(8080);
/*db.sync().then(result=>{
   
   // console.log(result);
}).catch(error=>{
   // console.log(error);
});
*/


