const loginData     =   require('../models/login');
const jwt           =   require('jsonwebtoken');
const common        =   require('../common');

exports.getPost =   (req,res,next) => {
    
    let commonClass          =     new common();
    //abcd.checkAuth;
   // let secKey      =   common.abcd();
    //console.log(secKey);
   // console.log(commonClass.secureKey);
    let response    =   {'ERROR':0,'MESSAGE':'','token':''};
    loginData.findAll({where: {
        Code:req.body.code,
        password:req.body.password
      }}).then(result=>{
        if (typeof result !== 'undefined' && result.length > 0) {
            response.MESSAGE    =   'Successfull Login redirect to dashboard page !';
            let payload         =   {subject:req.body.code}
            console.log(payload,commonClass.secureKey);
            let token           =   jwt.sign(payload,commonClass.secureKey);
            response.token       =   token;
        }else{
            response.ERROR      =   1;
            response.MESSAGE    =   'Code or Password doesnot match !';    
        }   
        res.json(response);
        //console.log(result);
    }).catch(error=>{
        response.ERROR      =   1;
        response.MESSAGE    =   error;  
        res.json(response);
    });
 
};