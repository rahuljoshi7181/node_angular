const Sequelize     =       require('sequelize');
const sequelize     =       require('../util/db');

const login         =       sequelize.define('UsersLogin',{
    id:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        allowNull:false,
        primaryKey:true
    },
    Code:{
        type:Sequelize.STRING,
        unique:true
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false
    },
    status:{
        type:Sequelize.STRING
    },
    LoginAttempt:Sequelize.STRING
    },{
        tableName: 'UsersLogin'
    }
);

module.exports  =   login;
