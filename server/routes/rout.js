const express   =   require('express');
const login     =   require('../controllers/login');
const common    =   require('../common');

let commonClass =   new common();

const rout      =   express.Router();

rout.post('/login',login.getPost);
//rout.post('/dashboard',commonClass.checkAuth);
rout.post('/validateUser',commonClass.checkAuth);
//commonClass.checkAuth

module.exports  =   rout;