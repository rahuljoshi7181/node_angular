import {NgModule} from '@angular/core'
import { Routes,RouterModule} from '@angular/router'
import { LoginComponent } from './login/login.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { AuthGuard } from './auth.guard'

const appRoutes: Routes    =   [
        {path:'',redirectTo: '/login',pathMatch: "full" },
        {path:'login',component:LoginComponent},
        {path:'dashboard',component:DashboardComponent,canActivate:[AuthGuard]}
]

@NgModule({
    imports:[
                RouterModule.forRoot(appRoutes)
            ],
    exports:[RouterModule]
})



export class AppRoutingModule{

}
