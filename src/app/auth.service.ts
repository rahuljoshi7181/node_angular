import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { common } from './common/common';
  
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _registerUrl  = '';
  api           = ''; 
  constructor(private http:HttpClient) { 

   this._registerUrl       =    common.apiUrl+'login';
  
  }

  loginUser(user){
    return this.http.post<any>(this._registerUrl,user);
  }

  checkDashboard(){
    let validateUser        =   common.apiUrl+'validateUser';
    let user                =   {};
    return this.http.post<any>(validateUser,user);
  }

  checkLogInUser(){
      return !!localStorage.getItem('token');
  }

  getToken() : any {
      return localStorage.getItem('token');
  }
}
