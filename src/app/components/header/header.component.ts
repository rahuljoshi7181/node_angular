import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Router } from '@angular/router';
import { AuthGuard } from '../../auth.guard';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl']
})
export class HeaderComponent implements OnInit {

  constructor(private router:Router,private _auth:AuthService) {

      this._auth.checkDashboard().subscribe(
        res=>{
          if(res.ERROR  ==  1){
            this.router.navigate(['/login']);
          }
        },
        error=>{
            alert(error);
        }
      )
   }

  ngOnInit() {
  }

  firstFunction(){
    console.log('TEST');
  } 
}
