import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {

  private loginUserData   = {};

  Code:string             =   '';
  password:string         =   '';
  constructor(private _auth:AuthService,private router:Router,) { 
   
   }

  ngOnInit() {
  }

  loginUser(regForm:any){
   
    this.loginUserData  = {code:regForm.controls.Code.value,password:regForm.controls.password.value};
    
    this._auth.loginUser(this.loginUserData)
    .subscribe(
      res => {
          if(res.ERROR  ==  0){
            localStorage.setItem('token',res.token);
            this.router.navigate(['/dashboard']);
          }else{
            alert(res.MESSAGE);
          }
      },
      error=> console.log(error)
    )
  }

  
}
